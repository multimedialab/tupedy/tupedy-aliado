import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApisService } from 'src/app/services/apis.service';
import { UtilService } from 'src/app/services/util.service';

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.page.html',
  styleUrls: ['./coupons.page.scss'],
})
export class CouponsPage implements OnInit {
  foods: any[] = [];
  dummy = Array(50);
  filterData: any[] = [];
  constructor(
    private router: Router,
    private api: ApisService,
    private util: UtilService
  ) {

  }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.getCoupons();
  }

  getCoupons() {
    this.dummy = [];
    this.filterData = [];
    this.api.getCoupons(localStorage.getItem('uid')).then((data) => {
      if (data) {
        data.forEach(el => {
          if (el.available.filter(x => x.id === localStorage.getItem('uid')).length !== 0) {
            this.filterData.push(el);
          }
        });
        this.foods = this.filterData;
      }
    }, error => {
      console.log(error);
      this.dummy = [];
      this.util.errorToast(this.util.translate('Something went wrong'));
    }).catch(error => {
      this.dummy = [];
      console.log(error);
      this.util.errorToast(this.util.translate('Something went wrong'));
    });
  }

  addnew() {
    this.router.navigate(['/add-new-coupons']);
  }

  foodsInfo(item) {
    // console.log(item);
    // const navData: NavigationExtras = {
    //   queryParams: {
    //     id: item.id
    //   }
    // };
    // this.router.navigate(['add-new-coupons'], navData);
    this.router.navigate(['/add-new-coupons']);
  }
}
function filterData(filterData: any) {
  throw new Error('Function not implemented.');
}

