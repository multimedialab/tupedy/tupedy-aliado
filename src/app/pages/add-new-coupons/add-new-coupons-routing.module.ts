import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNewCouponsPage } from './add-new-coupons.page';

const routes: Routes = [
  {
    path: '',
    component: AddNewCouponsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNewCouponsPageRoutingModule {}
