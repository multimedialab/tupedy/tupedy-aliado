import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNewCouponsPageRoutingModule } from './add-new-coupons-routing.module';

import { AddNewCouponsPage } from './add-new-coupons.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    AddNewCouponsPageRoutingModule
  ],
  declarations: [AddNewCouponsPage]
})
export class AddNewCouponsPageModule {}
